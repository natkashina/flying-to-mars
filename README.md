# Flying to Mars

## General info
This project is a questionnaire to fill out for a flight to Mars.
	
## Technologies
Project is created with:
* HTML
* CSS
* JavaScript
* Express
* Webpack

## Installation

To run this project, install it locally using [npm](https://www.npmjs.com/) and need use several terminals.

* In first terminal:

```
$ cd flying-to-mars
$ cd express
$ npm install
$ npm run dev
```

* In second terminal: 
```
$ cd flying-to-mars
$ cd ui
$ npm install
$ npm run build
$ npm start
```