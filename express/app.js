const express = require('express');
const serverPort = 3000;

const app = express();

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');

    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();

    app.options('*', (req, res) => {
        res.header('Access-Control-Allow-Methods', 'GET, PATCH, PUT, POST, DELETE, OPTIONS');
        res.send();
    });
});

app.listen(serverPort, () => {
    console.log("Server started on the port " + serverPort);
});

app.get('/birth-day', (req, res) => {
    res.set('Content-Type', 'application/json');
    res.sendFile(__dirname + "/shared/birth-day.json");
});

app.get('/marital-status', (req, res) => {
    res.set('Content-Type', 'application/json');
    res.sendFile(__dirname + "/shared/marital-status.json");
});

app.get('/education', (req, res) => {
    res.set('Content-Type', 'application/json');
    res.sendFile(__dirname + "/shared/education.json");
});

module.exports = app;