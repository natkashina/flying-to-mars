import { getDataForm } from "./index";

const urlBirthDay = "http://localhost:3000/birth-day";
const urlMaritalStatus = "http://localhost:3000/marital-status";
const urlEducation = "http://localhost:3000/education";

export function renderOptions(/*getDataForm*/) {
    fetch(urlBirthDay)
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            addOption("birthday-day", data, "dob-day");
            addOption("birthday-month", data, "dob-mouth");
            addOption("birthday-year", data, "dob-year");
        })
        .then(() => {
            getDataForm();
        });

    fetch(urlMaritalStatus)
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            addOption("marital-status", data, "marital-status");
        })
        .then(() => {
            getDataForm();
        });

    fetch(urlEducation)
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            addOption("educ", data, "education");
        })
        .then(() => {
            getDataForm();
        });

    function addOption(selectElement, data, arrayOptionsValue) {
        const select = document.getElementById(selectElement);

        select.innerHTML = "<option value=''>" + "</option>";

        data[arrayOptionsValue].forEach((option) => {
            const optionItem = document.createElement("option");
            optionItem.setAttribute("value", option);
            optionItem.innerText = option;

            select.appendChild(optionItem);
        });
    }
}
