import "./index.html";
import "./style/main.css";
import "./style/mobile.css";
import "./image/groovepaper.png";
import { renderOptions } from "./dataSelects";

const alphabet = {
    Ё: "YO",
    А: "a",
    Б: "B",
    В: "V",
    Г: "G",
    Д: "D",
    Е: "E",
    Ж: "H",
    З: "Z",
    И: "I",
    Й: "I",
    К: "K",
    Л: "L",
    М: "M",
    Н: "N",
    О: "O",
    П: "P",
    Р: "R",
    С: "S",
    Т: "T",
    У: "U",
    Ф: "F",
    Х: "H",
    Ц: "TS",
    Ч: "H",
    Ш: "SH",
    Щ: "SCH",
    Ъ: "",
    Ы: "I",
    Ь: "Y",
    Э: "E",
    Ю: "U",
    Я: "a",
    а: "a",
    б: "b",
    в: "v",
    г: "g",
    д: "d",
    е: "e",
    ж: "h",
    з: "z",
    и: "i",
    й: "i",
    к: "k",
    л: "l",
    м: "m",
    н: "n",
    о: "o",
    п: "p",
    р: "r",
    с: "s",
    т: "t",
    у: "u",
    ф: "f",
    х: "h",
    ц: "ts",
    ч: "h",
    ш: "sh",
    щ: "sch",
    ъ: "",
    ы: "i",
    ь: "y",
    э: "e",
    ю: "yu",
    я: "a",
    ё: "yo",
};

const createValidator = (callable, inputId, errorOutput) => () =>
    callable(inputId, errorOutput);

const state = {
    "sec-name-rus": {
        value: "",
        isValid: false,
        validator: createValidator(
            validateFullNameRus,
            "second-name-rus",
            "error-second-name-rus"
        ),
    },
    "check-surname": {
        checked: false,
        isValid: true,
        validator: createValidator(
            validateCheckboxPreviousSurname,
            "check-surname",
            "previous-surname"
        ),
    },
    "prev-surname": {
        value: "",
        isValid: true,
        validator: createValidator(
            validateFullNameRus,
            "previous-surname",
            "error-previous-surname"
        ),
    },
    "name-rus": {
        value: "",
        isValid: false,
        validator: createValidator(
            validateFullNameRus,
            "name-rus",
            "error-name-rus"
        ),
    },
    "mid-name": {
        value: "",
        isValid: false,
        validator: createValidator(
            validateFullNameRus,
            "middle-name",
            "error-middle-name"
        ),
    },
    "sec-name-latin": {
        value: "",
        isValid: false,
        validator: createValidator(
            validateFullNameLatin,
            "second-name-latin",
            "error-second-name-latin"
        ),
    },
    "name-latin": {
        value: "",
        isValid: false,
        validator: createValidator(
            validateFullNameLatin,
            "name-latin",
            "error-name-latin"
        ),
    },
    "dob-day": {
        value: "",
        isValid: false,
        validator: createValidator(validateSelect, "birthday-day", "error-dob"),
    },
    "dob-month": {
        value: "",
        isValid: false,
        validator: createValidator(
            validateSelect,
            "birthday-month",
            "error-dob"
        ),
    },
    "dob-year": {
        value: "",
        isValid: false,
        validator: createValidator(
            validateSelect,
            "birthday-year",
            "error-dob"
        ),
    },
    "marital-status": {
        value: "",
        isValid: false,
        validator: createValidator(
            validateSelect,
            "marital-status",
            "error-marital-status"
        ),
    },
    education: {
        value: "",
        isValid: false,
        validator: createValidator(validateSelect, "educ", "error-educ"),
    },
    tel: {
        value: "",
        isValid: false,
        validator: createValidator(
            validatePhoneNumber,
            "phone-number",
            "error-tel"
        ),
    },
    email: {
        value: "",
        isValid: false,
        validator: createValidator(validateEmail, "email", "error-email"),
    },
    "agree-to-fly": {
        isValid: false,
        validator: createValidator(
            validateCheckboxAgreeToFly,
            "agree-to-fly",
            "error-checkbox"
        ),
    },
};

const form = document.getElementById("form");

window.addEventListener("load", () => {
    renderOptions(getDataForm);
});

document.getElementById("check-surname").addEventListener("change", toggle);

form.addEventListener("change", (event) => {
    const submitButton = document.getElementById("grey-btn");

    handleChange(event);

    saveDataFrom(event);

    let disableSubmitButton = false;

    let arrayStateValues = Object.values(state);

    console.log(arrayStateValues);

    if (arrayStateValues.some((code) => code.isValid === false)) {
        disableSubmitButton = true;
    }

    if (disableSubmitButton) {
        submitButton.setAttribute("disabled", "disabled");
    } else {
        submitButton.removeAttribute("disabled");
    }

    console.log(state);
});

form.addEventListener("submit", (e) => {
    e.preventDefault();

    Array.prototype.forEach.call(form.elements, (input) => {
        input.value = "";
    });

    alert("Форма отправлена!");

    window.localStorage.clear();
});

export function getDataForm() {
    const checkboxPreviousSurname = document.querySelector(
        "[data-check='check-surname']"
    );
    const checkedCheckbox = window.localStorage.getItem("check-surname");

    for (let key in state) {
        const currentElem = window.localStorage.getItem(key);
        const currentValid = window.localStorage.getItem(`isValid_${key}`);

        if (currentElem == null) {
            continue;
        }

        if (key === "check-surname") {
            state["check-surname"].checked = JSON.parse(checkedCheckbox);
            checkboxPreviousSurname.checked = checkedCheckbox;
            toggle();
        }

        const inputElement = document.querySelector(`[data-name=${key}]`);

        if (inputElement != null) {
            state[key].value = currentElem;
            state[key].isValid = JSON.parse(currentValid);

            inputElement.value = currentElem;
        }
    }
}

function handleChange(event) {
    if (isCheckBox(event.target.id)) {
        state[event.target.dataset.check].validator();
        return;
    }

    state[event.target.dataset.name].value = event.target.value;
    state[event.target.dataset.name].validator();
}

function saveDataFrom(event) {
    if (!isCheckBox(event.target.id)) {
        window.localStorage.setItem(
            event.target.dataset.name,
            event.target.value
        );
        window.localStorage.setItem(
            `isValid_${event.target.dataset.name}`,
            state[event.target.dataset.name].isValid
        );
    }

    if (event.target.id === "agree-to-fly") {
        window.localStorage.setItem(
            `isValid_${event.target.dataset.check}`,
            state[event.target.dataset.check].isValid
        );
    }
}

function isCheckBox(elementId) {
    const elementCheckbox = document.getElementById(elementId);
    return elementCheckbox.type === "checkbox";
}

function validateFullNameRus(inputId, errorElement) {
    const regex = /[а-яА-Я]+$/;
    const errorMessage = "*Только символы русского языка";
    const validationResult = validate(
        inputId,
        regex,
        errorElement,
        errorMessage
    );

    if (validationResult) {
        if (inputId === "second-name-rus") {
            translateRusToLatin(inputId, "second-name-latin");
        } else if (inputId === "name-rus") {
            translateRusToLatin(inputId, "name-latin");
        }
    }
}

function validateFullNameLatin(inputId, errorElement) {
    const regex = /[A-Za-z]+$/;
    const errorMessage = "*Только символы латинского языка";

    return validate(inputId, regex, errorElement, errorMessage);
}

function validatePhoneNumber(inputId, errorElement) {
    const regex = /^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){11}(\s*)?$/;
    const errorMessage = "*Номер телефона должен содержать только 11 цифр";

    return validate(inputId, regex, errorElement, errorMessage);
}

function validateEmail(inputId, errorElement) {
    const regex = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/;
    const errorMessage = "*Email в формате: example@mail.com";

    return validate(inputId, regex, errorElement, errorMessage);
}

function validate(inputId, regex, errorElement, errorMessage) {
    const input = document.getElementById(inputId);

    if (input.value === "") {
        if (inputId === "previous-surname") {
            state[input.dataset.name].isValid = false;
            error(
                "*Если вы оставляете это поле пустым, то уберите флажок",
                errorElement
            );
            return;
        }
        errorMessage = "*Это поле обязательно для заполнения";
        error(errorMessage, errorElement);
    }

    const validationResult = regex.test(input.value);

    if (!validationResult) {
        error(errorMessage, errorElement);
    } else {
        hideError(errorElement);
    }

    state[input.dataset.name].isValid = validationResult;
    return validationResult;
}

function translateRusToLatin(inputRusId, inputLatinId) {
    const wordRus = document.getElementById(inputRusId).value;
    const wordLatin = document.getElementById(inputLatinId);
    let translateWord = "";

    for (let i = 0; i < wordRus.length; i++) {
        let letter = wordRus.charAt(i);

        if (alphabet[letter] === undefined) {
            return;
        }
        translateWord += alphabet[letter];
    }

    wordLatin.value = translateWord;

    state[wordLatin.dataset.name].value = translateWord;
    state[wordLatin.dataset.name].isValid = true;
    window.localStorage.setItem(wordLatin.dataset.name, translateWord);
}

function validateSelect(inputId, errorElement) {
    const selectElement = document.getElementById(inputId);
    const selectValue = selectElement.value;

    if (selectValue === "") {
        const errorMessage = "*Это поле обязательно для заполнения";
        error(errorMessage, errorElement);
        state[selectElement.dataset.name].isValid = false;
    } else {
        hideError(errorElement);
        state[selectElement.dataset.name].isValid = true;
    }
}

function validateCheckboxPreviousSurname(
    inputIdCheckbox,
    inputIdPreviousSurname
) {
    const checkbox = document.getElementById(inputIdCheckbox);
    const input = document.getElementById(inputIdPreviousSurname);
    const validationResult = validate; //функция валидации инпута возвращает true/false

    if (checkbox.checked) {
        state[checkbox.dataset.check].checked = true;
        state[input.dataset.name].isValid = false;
        window.localStorage.setItem(checkbox.dataset.check, checkbox.checked);
        if (validationResult) {
            window.localStorage.setItem(input.dataset.name, input.value);
        }
    } else {
        state[input.dataset.name].isValid = true;
        state[checkbox.dataset.check].checked = false;
        state[input.dataset.name].value = "";
        input.value = "";
        hideError("error-previous-surname");
        window.localStorage.removeItem(checkbox.dataset.check);
        window.localStorage.removeItem(input.dataset.name);
    }
}

function validateCheckboxAgreeToFly(inputId, errorElement) {
    const checkbox = document.getElementById(inputId);

    if (!checkbox.checked) {
        const errorMessage = "*Чтобы продолжить, установите этот флажок";
        error(errorMessage, errorElement);
    } else {
        hideError(errorElement);
    }
    state[checkbox.dataset.check].isValid = checkbox.checked;
}

function error(errorMessage, errorElement) {
    const spanError = document.getElementById(errorElement);
    spanError.innerText = errorMessage;
    spanError.style.display = "block";
}

function hideError(errorElement) {
    document.getElementById(errorElement).style.display = "none";
}

function toggle() {
    const checkbox = document.getElementById("check-surname");
    const previousSurname = document.getElementById("previous-surname");
    const previousSurnameLabel = document.querySelector(
        "label[for='previous-surname']"
    );
    previousSurname.style.display = checkbox.checked ? "block" : "none";
    previousSurnameLabel.style.display = checkbox.checked ? "block" : "none";
}
